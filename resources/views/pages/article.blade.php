@include('includes.Head')
@include('includes.Header')
<section class="sectionArticle">
        <div class="articleInfo">
                    <div class="articleCard">
                        <div class="articleImageContainer">
                            <img class="imgArticle" src="https://static.hitek.fr/img/actualite/ill_m/687047842/Fete-de-la-biere-10.jpg" class="card-img-top" alt="img">
                            <img class="imgArticle" src="https://resize-parismatch.lanmedia.fr/r/625,417,center-middle,ffffff/img/var/news/storage/images/paris-match/vivre/gastronomie/bieres-decouvrez-les-brasseurs-de-paris-1198154/20325485-1-fre-FR/Bieres-decouvrez-les-brasseurs-de-Paris.jpg" class="card-img-top" alt="img">
                            <img class="imgArticle" src="https://cdn.static01.nicematin.com/media/npo/mobile_1440w/2015/12/istock_000034973846large.jpg" class="card-img-top" alt="img">
                        </div>
                        <div class="cardBody">
                            <h5 class="card-title">{{$product->name}}</h5>
                            <p class="card-text">{{$product->description}}</p>
                            <p class="card-text">Categories: {{$product->subCategory->categorie->name}}</p>
                            <p class="card-text">Sub Categories: {{$product->subCategory->name}}</p>
                            <p class="card-text">Supports: {{$product->support->support}}</p>
                            <p class="card-text">Ajouté le: <small class="text-muted">{{date('d-m-Y', strtotime($product->created_at))}}</small></p>
                            <p class="card-text">Par: <a href="#"><small class="text-muted">{{$product->user->name}}</small></a></p>
                        </div>
                    </div>
                </div>
                <div class="btnContacter">
                    <button type="button" class="btn btn-warning">Contacter le Troquer</button>
                </div>
</section>
@include('includes.Footer')