@extends('layouts.User-layout')

@section('content')
<div class="container">
    <div class="row user-info">
        <div class="col-8 user-annonce">
            <h3>Vos annonces</h1>
            <div class="row user-annonceContainer">
                    <table class="table table-striped">
                          <thead>
                          <tr>
                              <td>Objets</td>
                              <td>Description</td>
                              <td>Options</td>                 
                          </tr>
                          </thead>
                          <tbody>
                          @foreach($products as $product)
                              <tr>
                                  <td>{{$product->name}}</td>
                                  <td>{{$product->description}}</td>
                                  <td class="btnSection">
                                    <a href="{{ route('article', $product->id)}}" class="btn btn-dark btnOption">Voir l'annonce</a>
                                    <a href="{{ route('product.edit',$product->id)}}" class="btn btn-primary btnOption">Edit</a>
                                        <form action="{{ route('product.destroy', $product->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btnOption" type="submit">Delete</button>
                                        </form>                                 
                                    </td>
                              </tr>
                          @endforeach
                          </tbody>
                      </table>
            </div>
        </div>
        <div class="col-4 profilInfos">
                <h3>Vos informations</h3>
                <div class="user-infos">
                    <h5>Pseudo : </h5>
                    <p>{{ Auth::user()->name }}</p>
                </div>
                <div class="user-infos">
                    <h5>Mail: </h5>
                    <p>{{ Auth::user()->email }}</p>
                </div>
        </div>
    </div>
</div>
@endsection