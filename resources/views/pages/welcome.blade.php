@extends('layouts.Default')
@section('content')
    <div class="sectionCarouselForm">
            <div class="CarouselContainer">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="https://picsum.photos/id/20/1000/400" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="https://picsum.photos/id/15/1000/400" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="https://picsum.photos/id/2/1000/400" alt="Third slide">
                            </div>
                        </div>
                    </div>
                </div>
                <form action="/find" method="POST" role="search">
                    {{ csrf_field() }}
                    <section class="sectionForm">
                        <div class="formHomeContainer">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Produit</label>
                                <input class="form-control" type="text" name="q">
                            </div>
                            <div class="form-group">
                                {{Form::label('categories_id', 'Categories')}}
                                <select class="form-control" name="categories_id">
                                    @foreach($categories as $categorie)
                                        <option name="categorie" value='{{$categorie->id}}'>{{$categorie->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                {{Form::label('supports_id', 'Supports')}}
                                <select class="form-control" name="support_id">
                                    @foreach($supports as $support)
                                        <option name="supports" value='{{$support->id}}'>{{$support->support}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Rechercher</button>
                    </section>
                </form>    
    </div>
    <div class="sectionAnnonces">
        <h1>Annonces</h1>
            
                <div class="form-group">
                    <div class="deck">
                        @foreach($products as $product)
                        <a href="{{ route('article', $product->id)}}">
                        <div class="card">
                            <img src="http://celebrities-entertainment.com/wp-content/uploads/2018/02/ACDC.jpg" class="card-img-top" alt="img">
                            <div class="card-body">
                                <h5 class="card-title">{{$product->name}}</h5>
                                <p class="card-text">{{$product->description}}</p>
                                <p class="card-text">Categories: {{$product->subCategory->categorie->name}}</p>
                                <p class="card-text">Sub Categories: {{$product->subCategory->name}}</p>
                                <p class="card-text">Supports: {{$product->support->support}}</p>
                                <p class="card-text">Ajouté le: {{date('d-m-Y', strtotime($product->created_at))}}</p>
                                <p class="card-text">Par: <a href="#" class="text-muted">{{$product->user->name}}</a></p>
                            </div>
                        </div>
                        </a>
                        @endforeach
                    </div>
                </div>
                 
    </div>
@stop
