<!doctype html>
<html>
  <head>
    @include('includes.Head')
  </head>

  <body>
    <div class="container>

      <header class="row">
        @include('includes.User-header')
      </header>

      <main>
        <div id="main" >
          @yield('content')
        </div>
      </main>

      <footer >
        @include('includes.User-footer')
      </footer>

    </div>
    <script src={{asset('js/app.js')}}></script>
  </body>
</html>