<!doctype html>
<html>
  <head>
    @include('includes.Head')
  </head>

  <body>
    <div class="container>

      <header class="row">
        @include('includes.Admin-header')
      </header>

      <main>
        <div id="main" >
          @yield('content')
          @include('pages.annonces')
        </div>
      </main>

      <footer >
        @include('includes.Admin-footer')
      </footer>

    </div>
    <script src={{asset('js/app.js')}}></script>
  </body>
</html>