<!doctype html>
<html>
  <head>
    @include('includes.Head')
  </head>

  <body>
    <div class="container>

      <header class="row">
        @include('includes.Header')
      </header>

      <main>
        <div id="main" >
          @yield('content')
          @include('pages.annonces')
        </div>
      </main>

      <footer >
        @include('includes.Footer')
      </footer>

    </div>
    <script src={{asset('js/app.js')}}></script>
  </body>
</html>
