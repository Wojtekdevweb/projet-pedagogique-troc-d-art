@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Liste de produits</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Nom du produit</td>
                    <td>Categorie</td>
                    <td>Sub categorie</td>
                    <td>Support</td>
                    <td>Description</td>
                    <td>Image</td>
                    <td colspan = 2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->subCategory->categorie->name}}</td>
                        <td>{{$product->subCategory->name}}</td>
                        <td>{{$product->support->support}}</td>
                        <td>{{$product->description}}</td>
                        <td>{{$product->user->name}}</td>
                        <td>
                            <a href="{{ route('product.edit',$product->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('product.destroy', $product->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            </div>

@endsection
