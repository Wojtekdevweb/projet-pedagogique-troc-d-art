@extends('layouts.app')
@include('includes.Head')
@include('includes.Header')
@section('content')
{{-- <h3 class="userNameTitle">Bienvenue<h3> --}}
    <div class="row formCreate">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3 titreFormCrate">Déposer une annonce</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="POST" action="{{ route('product.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nom produit</label>
                        <input type="text" class="form-control" name="name"/>
                    </div>

                    <div class="form-group">
                        {{Form::label('categories_id', 'Categories')}}
                        <select class="form-control" name="categories_id">
                            @foreach($categories as $categorie)
                                <option value='{{$categorie->id}}'>{{$categorie->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        {{Form::label('subcategories_id', 'Sub Categories')}}
                        <select class="form-control" name="sub_categorie_id">
                            @foreach($sub_categories as $sub_categorie)
                                <option value='{{$sub_categorie->id}}'>{{$sub_categorie->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        {{Form::label('supports_id', 'Supports')}}
                        <select class="form-control" name="support_id">
                            @foreach($supports as $support)
                                <option value='{{$support->id}}'>{{$support->support}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control"></textarea>
                    </div>

                    {{-- <div class="form-group">
                        <label for="picture">Image</label>
                        <input type="file" class="form-control imageImput" name="picture"/>
                    </div> --}}
                    
                        @csrf
                        <input type="file" name="picture">
                        <button type="submit" class="btn btn-primary btnAjout">Ajouter</button>
                </form>
            </div>
        </div>
    </div>
    @include('includes.Footer')
@endsection
