@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Sub Categories</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Nom</td>
                    <td>Categorie ID</td>
                    <td colspan=2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($sub_categories  as $sub_categorie)
                    <tr class="align-content-center">
                        <td class="align-middle">{{$sub_categorie->id}}</td>
                        <td class="align-middle">{{$sub_categorie->name}}</td>
                        <td class="align-middle">{{$sub_categorie->categorie_id}}</td>
                        <td class="align-middle">
                            <a href="{{ route('SubCategorie.create',$sub_categorie )}}" class="btn
                            btn-primary">Ajout</a>
                        </td>

                        <td class="align-middle">
                            <a href="{{ route('SubCategorie.edit', $sub_categorie )}}" class="btn
                            btn-warning">Edit</a>
                        </td>
                        <td class="align-middle">
                            <form action="{{ route('SubCategorie.destroy', $sub_categorie )}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
    </div>
    </div>
@endsection
