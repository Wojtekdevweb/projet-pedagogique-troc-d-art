@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Nouveau Support</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{ route('support.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="support">Support:</label>
                        <input type="text" class="form-control" name="support"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Nouveau Support</button>
                </form>
            </div>
        </div>
    </div>
@endsection
