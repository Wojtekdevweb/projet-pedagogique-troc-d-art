@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Supports</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Support</td>
                    <td colspan=2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($supports as $support)
                    <tr>
                        <td class="align-middle">{{$support->id}}</td>
                        <td class="align-middle">{{$support->support}}</td>
                        <td class="align-middle">
                            <a href="{{ route('support.create',$support )}}" class="btn btn-primary">Ajout</a>
                        </td>

                        <td class="align-middle">
                            <a href="{{ route('support.edit', $support )}}" class="btn
                            btn-warning">Edit</a>
                        </td>
                        <td class="align-middle">
                            <form action="{{ route('support.destroy', $support)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
    </div>
    </div>
@endsection
