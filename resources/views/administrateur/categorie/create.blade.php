@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Nouvelle categorie</h1>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{ route('categorie.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nom Categorie:</label>
                        <input type="text" class="form-control" name="name"/>
                    </div>

                    <div class="form-group">
                        <label for="picture">Image Url:</label>
                        <input type="text" class="form-control" name="picture"/>
                    </div>

                    <button type="submit" class="btn btn-primary">Nouvelle Categorie</button>
                </form>
            </div>
        </div>
    </div>
@endsection
