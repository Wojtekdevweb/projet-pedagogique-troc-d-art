@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Categories</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Nom</td>
                    <td>Image</td>
                    <td colspan=2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $categorie)
                    <tr class="align-content-center">
                        <td class="align-middle">{{$categorie->id}}</td>
                        <td class="align-middle">{{$categorie->name}}</td>
{{--                        <td>{{$categorie->picture}}</td>--}}
                        <td class="align-middle"><img src="{{$categorie->picture}}" class="img-thumbnail" width="150px"
                                 alt="image-responsive"></td>

                        <td class="align-middle">
                            <a href="{{ route('categorie.create',$categorie )}}" class="btn btn-primary">Ajout</a>
                        </td>

                        <td class="align-middle">
                            <a href="{{ route('categorie.edit', $categorie )}}" class="btn
                            btn-warning">Edit</a>
                        </td>
                        <td class="align-middle">
                            <form action="{{ route('categorie.destroy', $categorie)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
    </div>
    </div>
@endsection
