@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Update Categorie</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br />
            @endif
            <form method="post" action="{{ route('categorie.update', $categorie->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">

                    <label for="name">Nom Categorie:</label>
                    <input type="text" class="form-control" name="name" value={{ $categorie->name }} />
                </div>

                <div class="form-group">
                    <label for="picture">Image Url:</label>
                    <input type="text" class="form-control" name="picture" value={{ $categorie->picture }} />
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
