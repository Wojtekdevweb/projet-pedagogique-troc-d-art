<meta charset='utf-8'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<title>Troc'Art</title>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel='stylesheet' type='text/css' media='screen' href='/css/app.css'>
<script src="https://kit.fontawesome.com/95f249e43d.js"></script>
