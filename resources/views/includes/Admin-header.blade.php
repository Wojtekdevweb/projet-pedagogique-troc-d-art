<header>
  <div class=headrerSection>
      <div class="divHeaderLogo">
          {{-- <img src="/photos/35-40-large_default.jpg" class="headerLogo"> --}}
          {{-- <i class="fas fa-exchange-alt" class="headerLogo"></i> --}}
          <a href="{{ url('/') }}"><i class="fas fa-hands-helping" class="headerLogo"></i></a>
      </div>
      <div class="headerTitle">
          <h1>Troc D'Art</h1>
      </div>
      @guest
      <div class="headerButton">
          @if (Route::has('register'))
              <div class="nav-item btnLogin">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Devenir Troquer') }}</a>
              </div>
          @endif
      </div>
      @else
      <div class="sectionButtonHeader"></div>
      @endguest
  </div>
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">
              <li><a class="nav-link" href="{{ url('/') }}">Troc D'Art</a></li>
              <li><a class="nav-link" href="{{ route('product.create') }}">Deposer une annonce</a></li>
              @guest
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Se connecter') }}</a>
              </li>
          @else
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('user-home')}}">Mon Compte</a>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                         {{ __('Logout') }}
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>
          @endguest
              <li><a class="nav-link" href="{{ url('/test') }}">Aide</a></li>

          </ul>

          <!-- Right Side Of Navbar -->
          {{-- <form class="form-inline">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form> --}}
          <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->

          </ul>
      </div>
  </div>
</nav>
</header>

