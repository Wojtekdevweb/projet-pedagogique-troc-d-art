<footer>
  <nav class="navbar">
              <div class="footerMenu">
                  <ul class="navbar-nav mr-auto">
                      <li><a class="nav-link footerLinksMenu" href="{{ url('/') }}">Troc D'Art</a></li>
                      <li><a class="nav-link footerLinksMenu" href="{{ url('/create') }}">Deposer une annonce</a></li>
                      @guest
                      <li class="nav-item">
                          <a class="nav-link footerLinksMenu" href="{{ route('login') }}">{{ __('Mon compte') }}</a>
                      </li>                
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link footerLinksMenu dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>
      
                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('user-home')}}">Mon Compte</a>
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>
      
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
                      <li><a class="nav-link footerLinksMenu" href="{{ url('/test') }}">Aide</a></li>    
                  </ul>
              </div>
                      <div class="footerInfos">  
                          <p class="fotterP">Site web réalisé par l'agence AMATEURS DE LA BIÈRE.COM</p> 
                          <a class="footerMention" href="#">Mention Légales</a> 
                      </div>           
      </nav>
</footer>

