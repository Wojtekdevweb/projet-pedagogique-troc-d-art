@extends('layouts.app')
@include('includes.Head')
@include('includes.Header')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card sectionConfirmationWindow">
                <div class="card-header confirmationWindowHeader">Tableau de bord</div>

                <div class="card-body cofirmationWindow">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4>Bienvenue {{ Auth::user()->name }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.Footer')
@endsection