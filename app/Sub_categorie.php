<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_categorie extends Model
{
    protected $fillable = ['id', 'name', 'categorie_id'];

     public function categorie()
    {
        return $this->belongsTo('App\Categorie', 'categorie_id');
    }
}
