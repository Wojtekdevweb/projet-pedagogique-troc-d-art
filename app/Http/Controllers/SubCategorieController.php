<?php

namespace App\Http\Controllers;

use App\Sub_categorie;
use Illuminate\Http\Request;


class SubCategorieController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_categories = Sub_categorie::with('subCategory')->get();
        return view('administrateur.subCategorie.page', compact('sub_categorie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrateur.subCategorie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'categorie_id'=>'required',
        ]);

        $sub_categorie = new Sub_categorie ([
            'name' => $request->get('name'),
            'categorie_id' => $request->get('categorie_id'),
        ]);
        $sub_categorie->save();
        return redirect('/SubCategorie')->with('success', 'Sub Categorie creer!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sub_categorie  $sub_categorie
     * @return \Illuminate\Http\Response
     */
    public function show(Sub_categorie $sub_categorie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sub_categorie  $sub_categorie
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub_categorie = Sub_categorie::findOrFail($id);
        return view('administrateur.subCategorie.edit')->withSubCategorie($sub_categorie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sub_categorie  $sub_categorie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sub_categorie $sub_categorie)
    {
        $request->validate([
            'name'=>'required',
            'categorie_id'=>'required',
        ]);

        Sub_categorie::find($sub_categorie)->update($request->all());

        return redirect('/SubCategorie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sub_categorie  $sub_categorie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sub_categorie $sub_categorie)
    {
        $sub_categorie->delete();

        return redirect('/SubCategorie')->with('success', 'Sub Categorie deleted!');
    }
}
