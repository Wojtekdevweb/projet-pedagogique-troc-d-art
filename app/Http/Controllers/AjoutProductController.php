<?php

namespace App\Http\Controllers;

use App\Product;
use App\Sub_categorie;
use App\Categorie;
use App\Support;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AjoutProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {

        $products = Product::all();
        $categories = Categorie::all();
        $subcategories = Sub_categorie::all();
        $supports = Support::all();

        return view('produits.view', compact('products','categories', 'subcategories','supports' ));


    }

    public function create()
    {
        $products = Product::all();
        $categories = Categorie::all();
        $sub_categories = Sub_categorie::all();
        $supports = Support::all();

        return view('produits.create', compact('products', 'categories', 'sub_categories', 'supports'));
    }

    /*public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'sub_categorie_id' => 'required',
            'support_id' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('produits/create')
                ->withErrors($validator)
                ->withInput();
        }

        $product = Product::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'sub_categorie_id' => $request->get('sub_categorie_id'),
            'support_id' => $request->get('support_id'),


        ]);
        $product->save();
        return redirect('produits.view');
    }*/
}
