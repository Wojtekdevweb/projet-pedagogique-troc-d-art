<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Sub_categorie;
use App\Support;
use App\Product;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categorie::all();
        $subcategories = Sub_categorie::all();
        $supports = Support::all();
        $products = Product::all();

        return view('pages.annonces', compact('categories', 'subcategories','supports', 'products' ));


    }
    public function show($id)
    {
        return view('pages.article',['product' => Product::findOrFail($id)]);


    }
}