<?php

namespace App\Http\Controllers;

use App\Product;
use App\Categorie;
use App\Support;
use App\Sub_categorie;
use App\Picture;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $categories = Categorie::all();
        $sub_categories = Sub_categorie::all();
        $supports = Support::all();
        $pictures = Picture::all();
        //$user_id = auth()->user()->id;

        return view('produits.view', compact('products', 'categories', 'sub_categories','supports','pictures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $categories = Categorie::all();
        $sub_categories = Sub_categorie::all();
        $supports = Support::all();
        $pictures= Picture::all();
       
        return view('produits.create', compact('products', 'categories','sub_categories','supports', 'pictures'));

        //return view('produits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'sub_categorie_id' => 'required',
            'support_id' => 'required',
            'description' => 'required',
            // 'picture' => 'required|max:2048',

        ]);

        if ($validator->fails()) {
            return redirect()->route('product.create')
                ->withErrors($validator)
                ->withInput();
        }

        $product = Product::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'sub_categorie_id' => $request->get('sub_categorie_id'),
            'support_id' => $request->get('support_id'),
            'user_id' => auth()->user()->id,
            'picture' => $request->get('picture'),
        ]);
        $product->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $products = Product::all();
        return view('produits.view', compact('products'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::findOrFail($id);
        $sub_categories = Sub_categorie::findOrFail($id);
        return view('produits.edit', compact( 'product', 'sub_categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name'=>'required',
            'description' => 'require',
            'sub_categorie_id' => 'require',
            'support_id' => 'require',
            'user_id' => 'require',
        ]);

       Product::find($product)->update($request->all());

        return redirect('/produits.view');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->back();
    }
}
