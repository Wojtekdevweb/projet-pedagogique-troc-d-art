<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('pages.userHome');
    }

    public function list()
    {
        $user = Auth::user();
        $products = Product::where('user_id', $user->id)->get();
        return view('pages.userHome', ['products' =>$products]);
    }

}
