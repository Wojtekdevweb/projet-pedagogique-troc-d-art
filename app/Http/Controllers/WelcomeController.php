<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Support;
use App\Product;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categorie::all();
        $supports = Support::all();
        $products = Product::all();
        return view('pages.welcome', compact('categories', 'supports', 'products'));


    }
}