<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Sub_categorie;
use App\Support;
use App\Product;
use Illuminate\Http\Request;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Categorie::all();
        $subcategories = Sub_categorie::all();
        $supports = Support::all();

        return view('produits.view', compact('categories', 'subcategories','supports' ));


    }
}
