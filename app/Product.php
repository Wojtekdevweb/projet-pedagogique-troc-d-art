<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['id', 'name', 'description', 'sub_categorie_id', 'support_id', 'user_id'];

    public function subCategory(){
        return $this->belongsTo('App\Sub_categorie', 'sub_categorie_id');
    } 
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function support()
    {
        return $this->belongsTo('App\Support', 'support_id');
    }
}

