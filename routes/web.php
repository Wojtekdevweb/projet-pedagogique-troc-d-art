<?php
use App\Product;
use App\Categorie;
use App\Support;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::get('/client', 'ClientController@index')->name('client');

Route::resource('product', 'ProductController')
    ->middleware('auth');
  
Route::resource('categorie', 'CategorieController');
Route::resource('support', 'SupportController');
Route::resource('SubCategorie', 'SubCategorieController');

Route::get('/user-home', 'UserHomeController@list')->name('user-home');

Auth::routes();

Route::get('/confirmation', function () {
    return view('auth.confirmation');
});

Route::get('/administrateur.dashboard', 'AdminController@login')->name('dashboard');

Route::get('/article/{id}', 'ArticleController@show')->name('article');

Route::post('/find',function(){
    $q = Input::get ( 'q' );
    // $categorySelect = Input::get('categorie');
    // $supportSelect = Input::get('supports');
    $product = Product::where('name','LIKE','%'.$q.'%')->get();
    if(count($product) > 0)
        return view('search')->withDetails($product)->withQuery ( $q );
    else return view ('search')->withMessage('No Details found. Try to search again !');
});