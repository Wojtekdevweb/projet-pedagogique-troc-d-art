<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre');
            $table->text('content');
            $table->bigInteger('from_id')->unsigned();
            $table->foreign('from_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->bigInteger('to_id')->unsigned();
            $table->foreign('to_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->boolean('read')->default(0)->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
