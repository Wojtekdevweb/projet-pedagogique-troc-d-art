-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  lun. 16 sep. 2019 à 10:32
-- Version du serveur :  5.7.27-0ubuntu0.18.04.1
-- Version de PHP :  7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `TrocArt`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `picture`, `created_at`, `updated_at`) VALUES
(4, 'Sculpture', 'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F64897803%2F85805756037%2F1%2Foriginal.20190704-210145?w=1000&auto=compress&rect=0%2C21%2C1536%2C768&s=2d476953be042ae0392f203c4b3f678d', '2019-09-06 08:50:33', '2019-09-06 08:50:33'),
(5, 'Art Visual', 'https://p7.storage.canalblog.com/70/35/420257/56955540.jpg', '2019-09-06 09:12:57', '2019-09-06 09:12:57'),
(6, 'Musique', 'https://www.ville-lomme.fr/var/www/storage/images/mediatheque/ecole-de-musique-et-de-danse2/127531-1-fre-FR/Ecole-de-musique-et-de-danse_news_image_top.jpg', '2019-09-06 09:14:42', '2019-09-06 09:14:42'),
(7, 'Littératures', 'https://www.myjewishlearning.com/wp-content/uploads/2007/09/books.jpg', '2019-09-06 09:15:29', '2019-09-06 09:15:29'),
(8, 'Cinéma', 'https://frenchmorning.com/wp-content/uploads/2017/03/3078493320_aa0a7ec1bd_b-696x378.jpg', '2019-09-06 09:16:29', '2019-09-06 09:16:29');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_id` bigint(20) UNSIGNED NOT NULL,
  `to_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_03_074700_create_supports_table', 1),
(4, '2019_09_03_074741_create_messages_table', 1),
(5, '2019_09_03_074824_create_categories_table', 1),
(6, '2019_09_03_074901_create_sub_categories_table', 1),
(7, '2019_09_03_075000_create_products_table', 1),
(8, '2019_09_03_075001_create_pictures_table', 1),
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2019_09_03_074700_create_supports_table', 1),
(12, '2019_09_03_074741_create_messages_table', 1),
(13, '2019_09_03_074824_create_categories_table', 1),
(14, '2019_09_03_074901_create_sub_categories_table', 1),
(15, '2019_09_03_075000_create_products_table', 1),
(16, '2019_09_03_075001_create_pictures_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_categorie_id` bigint(20) UNSIGNED NOT NULL,
  `support_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `sub_categorie_id`, `support_id`, `user_id`, `created_at`, `updated_at`) VALUES
(9, 'AC-DC', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.', 2, 6, 1, '2019-09-12 06:41:13', '2019-09-12 06:41:13'),
(10, 'AC-DC', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.', 2, 6, 1, '2019-09-12 06:41:20', '2019-09-12 06:41:20'),
(11, 'AC-DC', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.', 2, 6, 1, '2019-09-12 06:41:21', '2019-09-12 06:41:21'),
(12, 'AC-DC', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.', 2, 6, 1, '2019-09-12 06:41:23', '2019-09-12 06:41:23'),
(13, 'ZZ-TOP', 'Macaque proboscis baboon langur africa old world mandrill adult colobus mammal. Macaque colobus old world africa adult mandrill baboon mammal proboscis langur.', 2, 7, 1, '2019-09-12 06:59:33', '2019-09-12 06:59:33'),
(15, 'Rambo', 'Le Dévastateur au Québec (First Blood)a, est un film d\'action américain réalisé par Ted Kotcheff, sorti en 1982.', 5, 7, 1, '2019-09-13 06:11:17', '2019-09-13 06:11:17');

-- --------------------------------------------------------

--
-- Structure de la table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categorie_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `categorie_id`, `created_at`, `updated_at`) VALUES
(1, 'jazz', 6, '2019-09-06 11:41:22', '2019-09-06 11:41:22'),
(2, 'Rock', 6, '2019-09-06 11:42:46', '2019-09-06 11:42:46'),
(3, 'Blues', 6, '2019-09-06 11:42:58', '2019-09-06 11:42:58'),
(4, 'Policier', 8, '2019-09-06 11:43:36', '2019-09-06 11:43:36'),
(5, 'Aventure', 8, '2019-09-06 11:43:54', '2019-09-06 11:43:54'),
(6, 'Comedie', 8, '2019-09-06 11:44:11', '2019-09-06 11:44:11'),
(7, 'Dessins', 5, '2019-09-06 11:45:02', '2019-09-06 11:45:02'),
(8, 'Peinture', 5, '2019-09-06 11:45:16', '2019-09-06 11:45:16'),
(9, 'Photo', 5, '2019-09-06 11:45:28', '2019-09-06 11:45:28'),
(10, 'poésie', 7, '2019-09-06 11:45:51', '2019-09-06 11:45:51'),
(11, 'Livre', 7, '2019-09-06 11:46:10', '2019-09-06 11:46:10'),
(12, 'bd', 7, '2019-09-06 11:46:26', '2019-09-06 11:46:26');

-- --------------------------------------------------------

--
-- Structure de la table `supports`
--

CREATE TABLE `supports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `support` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `supports`
--

INSERT INTO `supports` (`id`, `support`, `created_at`, `updated_at`) VALUES
(1, 'Dvd', '2019-09-06 10:25:01', '2019-09-06 10:25:01'),
(3, 'VHS', '2019-09-06 10:25:41', '2019-09-06 10:25:41'),
(4, 'Vinyle', '2019-09-06 10:25:53', '2019-09-06 10:25:53'),
(5, 'Super-8', '2019-09-06 10:26:16', '2019-09-06 10:26:16'),
(6, 'CD', '2019-09-06 10:26:46', '2019-09-06 10:26:46'),
(7, 'Cassette', '2019-09-06 10:26:56', '2019-09-06 10:26:56');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Wojtek', 'wojtek@gmail.com', NULL, '$2y$10$SaFN.OTOtVuo6RJ50t77IOtXRgX1GWaPhFEi8V0Za7TU8odwjWP4W', NULL, '2019-09-11 10:45:40', '2019-09-11 10:45:40');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_from_id_foreign` (`from_id`),
  ADD KEY `messages_to_id_foreign` (`to_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pictures_product_id_foreign` (`product_id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_sub_categorie_id_foreign` (`sub_categorie_id`),
  ADD KEY `products_support_id_foreign` (`support_id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Index pour la table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_categorie_id_foreign` (`categorie_id`);

--
-- Index pour la table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_from_id_foreign` FOREIGN KEY (`from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_to_id_foreign` FOREIGN KEY (`to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_sub_categorie_id_foreign` FOREIGN KEY (`sub_categorie_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_support_id_foreign` FOREIGN KEY (`support_id`) REFERENCES `supports` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
